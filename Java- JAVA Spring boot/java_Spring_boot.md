`DAO` is an abstraction of **data persistence**.
`Repository` is an abstraction of **a collection of objects**.

`DAO` would be considered closer to the database, often table-centric.

### DAO - data access object (in other words - object used to access data)

`Repository` could be implemented using `DAO`'s, but you wouldn't do the opposite.



`Note that` the `Repository` is using `Domain Model terms` (not `DB terms` - nothing related to how data is persisted anywhere)

`DAO pattern` is a way of generating DAL, where typically, each domain entity has its own DAO `Data Access layer`. 

For example, `User` and `UserDao`, `Appointment` and `AppointmentDao`, etc

Like `DAO,` `Repository` pattern is also a way achieving `DAL`.

The `Entity/Model` basically doesn't have any responsibility and its just a `POJO (Plain Old Java Object)` with fields and` getters `and `setters`.

A `Repository` is an object which provides basic CRUD operation on **one** `Entity`

The `service layer` is there to provide logic to operate on the data sent to and from the` DAO` and `the client`. Very often these 2 pieces will be bundled together into the same module, and occasionally into the same code, but you'll still see them as distinct logical entities.



https://stackoverflow.com/questions/25203564/jpa-repository-vs-entity-responsbilities

https://stackoverflow.com/questions/40902280/what-is-the-recommended-project-structure-for-spring-boot-rest-projects

